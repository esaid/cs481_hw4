import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/animation.dart';

void main() {
  runApp(MyAnimation());
}

class MyAnimation extends StatefulWidget {
  AnimationState createState() => AnimationState();
}

class AnimationState extends State<MyAnimation> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override 
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 5), vsync: this);
    //animation = Tween<double>(begin: 0, end: 300).animate(controller)
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
    ..addStatusListener((status) {
      if(status == AnimationStatus.completed) {
        controller.reverse();
      } 
      else if(status == AnimationStatus.dismissed) {
        controller.forward();
      }
    })
    ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override 
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override 
  void dispose() {
    controller.dispose();
    super.dispose();
  }

}

 class AnimatedLogo extends AnimatedWidget {

    static final opacityTween = Tween<double>(begin: 0.1, end: 1);
    static final sizeTween = Tween<double>(begin: 0, end: 300);
    AnimatedLogo({Key key, Animation<double> animation}) : super(key: key, listenable: animation);

    
    Widget build(BuildContext context) {
      final animation = listenable as Animation<double>;
      return Center(
        child: Opacity(
          opacity: opacityTween.evaluate(animation),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            height: sizeTween.evaluate(animation),
            width: sizeTween.evaluate(animation),
            child: FlutterLogo(),
          ),
        ),
      );
   }  
 }